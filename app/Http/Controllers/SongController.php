<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\MediaType;
use App\Track;

class SongController extends Controller
{
	public function displayDropdowns(){

		$genre = new Genre();
		$media_type = new MediaType();

		// var_dump($genre->all());
		// exit();

		// return "In SongController";
		return view('search_form', [
			// 'firstName' => 'Tommy',
			// 'lastName' => 'Trojan',
			'genres' => $genre->all(),
			'media_types' => $media_type->all()
		]);
	}

	public function displayResults(Request $req){

		$track_name = $req->input('track_name');
		$genre_id = $req->input('genre_id');
		$media_type_id = $req->input('media_type_id');

		$tracks = new Track();

		$results = $tracks->select('tracks.name AS track', 'genres.name AS genre', 'media_types.name AS media_type')
		->leftJoin('genres', 'tracks.genre_id', '=', 'genres.genre_id')
		->leftJoin('media_types', 'tracks.media_type_id', '=', 'media_types.media_type_id');

		if ( isset($track_name) ) {
			$results->where("tracks.name", "like", "%$track_name%");
		}

		if ( isset($genre_id) ) {
			$results->where("tracks.genre_id", $genre_id);
		}

		if ( isset($media_type_id) ) {
			$results->where("tracks.media_type_id", $media_type_id);
		}

		return view('search_results', [
			'results' => $results->get()
		]);
	}
}
